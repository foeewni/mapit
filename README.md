MapIt PHP Wrapper
=

This is a very simple PHP wrapper for MySociety's excellent [MapIt](http://mapit.mysociety.org/) service.

This is not in any way endorsed by MySociety.

Usage
-
Use composer:
```
composer require foeewni/mapit
```

Then see the example.php file for example usage - it's very simple!