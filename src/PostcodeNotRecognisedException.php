<?php


namespace foeewni\mapit;


class PostcodeNotRecognisedException extends \Exception
{
    public function __construct($postcode)
    {
        parent::__construct("The postcode $postcode was not recognised by MapIt");
    }

}