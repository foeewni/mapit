<?php


namespace foeewni\mapit;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class MapIt
{

    /**
     * @var Client
     */
    protected $httpClient;

    public function __construct($baseUrl = 'https://mapit.mysociety.org/', $options = [])
    {
        $options['base_uri'] = $baseUrl;
        $this->httpClient = new Client($options);
    }

    /**
     * Fetch data for a given postcode
     * @see https://mapit.mysociety.org/
     *
     * @param $postcode
     * @return array
     * @throws PostcodeNotRecognisedException
     *   If the postcode wasn't recognised by MapIt
     */
    public function postcode($postcode)
    {
        $postcode = $this->normalisePostcode($postcode);

        try {
            return $this->doRequest("postcode/$postcode");

        } catch(ClientException $exception)
        {
            // Try to convert a postcode problem into a more specific exception
            if ($exception->getResponse()->getStatusCode() == '400')
            {
                $body = json_decode($exception->getResponse()->getBody(),true);
                if (isset($body['error'])) {
                    if (strpos($body['error'], "Postcode") !== FALSE
                        && strpos($body['error'], "is not valid") !== FALSE)
                    {
                        throw new PostcodeNotRecognisedException($postcode);
                    }
                }

            }

            throw $exception;
        }
    }

    /**
     * Fetch data for a given partial postcode
     * @see https://mapit.mysociety.org/
     *
     * @param $postcode
     * @return array
     */
    public function postcodePartial($postcode)
    {
        $postcode = $this->normalisePostcode($postcode);
        return $this->doRequest("postcode/partial/$postcode");
    }

    /**
     * Fetch areas for a given postcode
     *
     * For the full list of possible area types
     * @see https://mapit.mysociety.org/postcode/SW1A1AA.html
     * for the bracketed three letter acronymns
     *
     * @param $postcode
     * @param array $areaTypes
     *  The area types to filter to, or [] to show all
     * @return array
     *
     */
    public function getAreasFor($postcode, $areaTypes = [])
    {
        $fullData = $this->postcode($postcode);

        $result = [];

        foreach($fullData['areas'] as $areaKey => $areaData)
        {
            if (count($areaTypes)) {
                // For some reason, this doesn't work using array_search
                foreach ($areaTypes as $type) {
                    if ($type == $areaData['type']) {
                        $result[$type] = $areaData;
                    }
                }
            } else {
                $result[$areaData['type']] = $areaData;
            }
        }

        return $result;
    }

    /**
     * Format a postcode for the API
     *
     * @param $postcode
     * @return mixed
     */
    protected function normalisePostcode($postcode)
    {
        return str_replace(' ','',$postcode);
    }

    /**
     * Make a request and decode it
     *
     * @param $url
     * @return mixed
     */
    protected function doRequest($url)
    {

        $result = $this->httpClient->get($url);

        return json_decode($result->getBody(),true);
    }

}