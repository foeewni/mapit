<?php

use foeewni\mapit\MapIt;

include 'vendor/autoload.php';

$m = new MapIt();

// Fetch all the data for a given postcode
//print_r($m->postcode('SW9 0HP'));

// Fetch just the european region and westminster constituency
//print_r($m->getAreasFor('SW9 0HP',['EUR','WMC']));

print_r($m->getAreasFor('SW9 0HP',[]));

// Will throw a PostcodeNotRecognised exception
//print_r($m->getAreasFor('ABC',[]));